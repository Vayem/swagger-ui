FROM node:12-alpine

EXPOSE 3000
WORKDIR /

COPY package*.json ./
RUN npm install
COPY . .

CMD [ "node", "index.js" ]