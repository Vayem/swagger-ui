const express = require("express");
const app = express();
const swaggerUi = require("swagger-ui-express");
const port = 3000;

const options = require("./options.json");

app.use(swaggerUi.serve, swaggerUi.setup(null, options));

app.listen(port, () => console.log(`App listening on port ${port}`));
